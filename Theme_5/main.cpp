//
// Created by Louis on 08/11/2020.
//

#include "../tp5.h"
#include "headers/B.h"
#include "headers/T.h"
int theme_5();

#ifdef THEME_5

int main()
{
    theme_5();
    return 0;
}
#endif

int theme_5()
{
    {
        B b = B();
        A a = A();
        B b2 = B(a);
        B b3 = B(b);
    }

    {
        Sess sess = Sess();
        Sess sess1 = Sess();
        Sess sess2 = Sess();
        {
            Sess sess3 = Sess();
            Sess sess4 = Sess();
        }
        Sess sess3 = Sess();
        Sess sess4 = Sess();
    }

    T_theme5 t = T_theme5();
    T_theme5 t2 = T_theme5(new U());
    T_theme5 t3 = T_theme5(t);



    return 0;
}