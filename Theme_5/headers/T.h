//
// Created by Louis on 08/11/2020.
//

#ifndef TPDUOCPP_T_THEME5_H
#define TPDUOCPP_T_H
#include "../headers/U.h"

class T_theme5 {
    U * u{};
public:
    T_theme5(U *u);
    T_theme5();
    T_theme5(const T_theme5 &);
    virtual ~T_theme5();

};


#endif //TPDUOCPP_T_THEME5_H
