//
// Created by Louis on 08/11/2020.
//

#ifndef TPDUOCPP_B_H
#define TPDUOCPP_B_H
#include "A.h"

class B {
    A a;
public:
    B(const A &a);

    B();

    B(const B &);



};


#endif //TPDUOCPP_B_H
