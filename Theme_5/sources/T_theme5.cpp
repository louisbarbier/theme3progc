//
// Created by Louis on 08/11/2020.
//

#include "../headers/T.h"


T_theme5::T_theme5(U *u) : u(u) {}
T_theme5::T_theme5() {
    T_theme5::u = new U();
}
T_theme5::T_theme5(const T_theme5 & t)
{
    *T_theme5::u = *t.u;

}
T_theme5::~T_theme5() {
    std::cout<<"Destructor of T on theme 5"<<std::endl;
    delete T_theme5::u;
}