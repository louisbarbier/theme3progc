//
// Created by Louis on 08/11/2020.
//

#include "../headers/Sess.h"
int Sess::object_count = 0;
Sess::Sess() {
    Sess::object_count++;
    std::cout << "Sess::(Constructor) There is:" << object_count << " object(s)" << std::endl;
}

Sess::~Sess() {
    Sess::object_count--;
    std::cout << "Sess::(Destructor) There is:" << object_count << " object(s)" << std::endl;
}
