//
// Created by Louis on 08/11/2020.
//

#include "../headers/B.h"

B::B(const A &a) : a(a) {}

B::B() {
    a = A();
}

B::B(const B & b) {
    B::a = b.a;

}
