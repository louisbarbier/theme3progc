#include "../tp5.h"
#include "headers/complexe.h"
#include "headers/Vector.h"

#ifdef THEME_4
int theme_4();
int main() {

    theme_4();
    return 0;
}
#endif


int theme_4()
{
    Complexe c(8,4);
    Complexe c2(2,2);
    Complexe c1(c);
    Complexe test;
    Complexe test2;

    test=c + c2;

    std::cout  << test.getReel() << " + " << test.getImaginaire() << "i"<< "\n" <<std::endl;

    test2=test<<c2;

    std::cout  << test2.getReel() << " + " << test2.getImaginaire() << "i"<< "\n" << std::endl;

    std::cout << "Partie vector" <<std::endl;
    Vector default_vector = Vector();

    int tableau[5] = {1,2,3,4,5};

    Vector vector = Vector(tableau,5);

    std::cout<<"Vetcor default:"<<std::endl;
    default_vector.display();
    std::cout<<"Vector:"<<std::endl;
    vector.display();

    std::cout<<"Addition:"<<std::endl;
    Vector vect = default_vector+vector;
    vect.display();


    std::cout<<"Subtraction:"<<std::endl;
    (default_vector-vector).display();

    std::cout<<"Division:"<<std::endl;
    (default_vector/vector).display();

    std::cout<<"Multiplication:"<<std::endl;
    (default_vector*vector).display();

    std::cout<<"Equal:"<<std::endl;
    (default_vector=vector).display();

    std::cout<<"<<:"<<std::endl;
    (default_vector<<vector).display();

    std::cout<<">>:"<<std::endl;
    (default_vector>>vector).display();

    return 0;


}