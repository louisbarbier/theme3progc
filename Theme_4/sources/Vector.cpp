//
// Created by Louis on 08/11/2020.
//

#include "../headers/Vector.h"
#include <cstdlib>

Vector::Vector(const int *dynamicTable, int size) : size(size) {
    Vector::dynamic_table = (int*)malloc(sizeof(int) * size);
    for(int i=0;i<size;++i)
    {
        Vector::dynamic_table[i] = dynamicTable[i];
    }
}


Vector::Vector() {
    Vector::dynamic_table = (int*) malloc(10*sizeof(int));
    Vector::size = 10;
    for(int i=0;i<10;++i)
    {
        Vector::dynamic_table[i] = 1;
    }
}

Vector operator+(Vector & vector1, Vector & vector2) {
    /**
     * On imagine que l'on va additionner indice de tableau à indice à tableau
     */
     Vector result;
     if (vector1.size == vector2.size)
     {
         result.dynamic_table= (int*)malloc(sizeof(int) * vector1.size);
         result.size = vector1.size;
         for(int i=0;i<vector1.size;++i)
         {
             result.dynamic_table[i] = vector1.dynamic_table[i] + vector2.dynamic_table[i];
         }
         return result;
     }else{
         int size = vector1.size > vector2.size ? vector1.size : vector2.size;


         result.dynamic_table = (int*)malloc(sizeof(int) * size);
         result.size = size;

         for(int i=0;i<size;++i)
         {
             if(i>= vector1.size)
             {
                 result.dynamic_table[i] = vector2.dynamic_table[i];
             }else if(i>= vector2.size)
             {
                 result.dynamic_table[i] = vector1.dynamic_table[i];
             }else{
                 result.dynamic_table[i] = vector1.dynamic_table[i] + vector2.dynamic_table[i];
             }

         }
         return result;
     }
}

Vector operator-(Vector & vector1, Vector & vector2) {
    /**
     * On imagine que l'on va additionner indice de tableau à indice à tableau
     */

    Vector result;
    if (vector1.size == vector2.size)
    {
        result.dynamic_table= (int*)malloc(sizeof(int) * vector1.size);
        result.size = vector1.size;
        for(int i=0;i<vector1.size;++i)
        {
            result.dynamic_table[i] = vector1.dynamic_table[i] - vector2.dynamic_table[i];
        }
        return result;
    }else{
        int size = vector1.size > vector2.size ? vector1.size : vector2.size;
        result.dynamic_table = (int*)malloc(sizeof(int) * size);
        result.size = size;
        for(int i=0;i<size;++i)
        {
            if(vector1.dynamic_table[i] == NULL)
            {
                result.dynamic_table[i] = vector2.dynamic_table[i];
            }else if(vector2.dynamic_table[i] == NULL)
            {
                result.dynamic_table[i] = vector1.dynamic_table[i];
            }else{
                result.dynamic_table[i] = vector1.dynamic_table[i] - vector2.dynamic_table[i];
            }

        }

        return result;
    }
}

Vector operator/(Vector & vector1, Vector & vector2) {
    /**
     * On fait une division de int. Le mieux aurait été d'utiliser auto ou
     * de trouver un moyen de changer le type.......
     */
    Vector result;
    if (vector1.size == vector2.size)
    {
        result.dynamic_table= (int*)malloc(sizeof(int) * vector1.size);
        result.size = vector1.size;
        for(int i=0;i<vector1.size;++i)
        {
            result.dynamic_table[i] = vector1.dynamic_table[i] / vector2.dynamic_table[i];
        }
        return result;
    }else{
        int size = vector1.size > vector2.size ? vector1.size : vector2.size;
        result.dynamic_table = (int*)malloc(sizeof(int) * size);
        result.size = size;
        for(int i=0;i<size;++i)
        {
            if(vector1.dynamic_table[i] == NULL)
            {
                result.dynamic_table[i] = vector2.dynamic_table[i];
            }else if(vector2.dynamic_table[i] == NULL)
            {
                result.dynamic_table[i] = vector1.dynamic_table[i];
            }else{
                result.dynamic_table[i] = vector1.dynamic_table[i] / vector2.dynamic_table[i];
            }

        }
        return result;
    }
}

Vector operator*(Vector & vector1, Vector &vector2)  {
    Vector result;
    if (vector1.size == vector2.size)
    {
        result.dynamic_table= (int*)malloc(sizeof(int) * vector1.size);
        result.size = vector1.size;
        for(int i=0;i<vector1.size;++i)
        {
            result.dynamic_table[i] = vector1.dynamic_table[i] * vector2.dynamic_table[i];
        }
        return result;
    }else{
        int size = vector1.size > vector2.size ? vector1.size : vector2.size;
        result.dynamic_table = (int*)malloc(sizeof(int) * size);
        result.size = size;
        for(int i=0;i<size;++i)
        {
            if(vector1.dynamic_table[i] == NULL)
            {
                result.dynamic_table[i] = vector2.dynamic_table[i];
            }else if(vector2.dynamic_table[i] == NULL)
            {
                result.dynamic_table[i] = vector1.dynamic_table[i];
            }else{
                result.dynamic_table[i] = vector1.dynamic_table[i] * vector2.dynamic_table[i];
            }

        }
        return result;
    }
}

Vector operator<<(Vector & vector1, Vector & vector2) {
    /**
     * On concatene le contenu de vector1 avant vector2
     */
    Vector result;
    result.size = vector1.size + vector2.size;
    result.dynamic_table = (int*)malloc(sizeof(int) * result.size);
    for (int i=0;i<result.size;++i)
    {
        if(i<vector1.size)
        {
            result.dynamic_table[i] = vector1.dynamic_table[i];
        }else{
            result.dynamic_table[i] = vector2.dynamic_table[i - vector1.size];
        }
    }
    return result;
}

Vector operator>>(Vector & vector1, Vector & vector2) {
    return vector2<<vector1;
}

//Vector Vector::operator=(Vector & vector) {
//    Vector::size = vector.size;
//    for(int i=0;i<vector.size;++i)
//    {
//        *(Vector::dynamic_table + i) = *(vector.dynamic_table + i);
//    }
//
//    return *this;
//}

void Vector::display() {
    for(int i=0;i<Vector::size;++i)
    {
        std::cout<<Vector::dynamic_table[i]<<std::endl;
    }

}

Vector::~Vector() {
    delete [] Vector::dynamic_table;
}

int *Vector::getDynamicTable() const {
    return dynamic_table;
}

void Vector::setDynamicTable(int *dynamicTable) {
    dynamic_table = dynamicTable;
}

int Vector::getSize() const {
    return size;
}

void Vector::setSize(int size) {
    Vector::size = size;
}
