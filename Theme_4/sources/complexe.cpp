#include "../headers/complexe.h"
#include<cmath>
#include <vector>

Complexe::Complexe(int reel, int imaginaire)
{
    // Constructeur
    std::cout << "Constructeur" << std::endl;
    this->_imaginaire = imaginaire;
    this->_reel = reel;
}
Complexe::Complexe(Complexe const &complexe)
{
    // Constructeur
    std::cout << "Constructeur par recopie" << std::endl;
    this->_imaginaire = complexe.getImaginaire();
    this->_reel = complexe.getReel();
}
Complexe::~Complexe()
{
    // Desctructeur
    std::cout << "Destructor" << std::endl;
}

void Complexe::setReel(int reel)
{
    _reel = reel;
}

void Complexe::setImaginaire(int imaginaire)
{
    _imaginaire = imaginaire;
}

int Complexe::getReel() const
{
    return _reel;
}

int Complexe::getImaginaire() const
{
    return _imaginaire;
}

void Complexe::addition(int imaginaire, int reel)
{
    setImaginaire(this->getImaginaire() + imaginaire);
    setReel(this->getReel() + reel);
}


void Complexe::soustraction(int imaginaire, int reel)
{
    setImaginaire(this->getImaginaire() - imaginaire);
    setReel(this->getReel() - reel);
}

void Complexe::division(int imaginaire, int reel)
{
    if (imaginaire == 0 || reel == 0 || this->getImaginaire() == 0 || this->getReel() == 0)
    {
        std::cerr << "Division par 0 impossible" << std::endl;
        return;
    }
    setImaginaire(this->getImaginaire() / imaginaire);
    setReel(this->getReel() / reel);
}

void Complexe::multiplication(int imaginaire, int reel)
{
    setImaginaire(this->getImaginaire() * imaginaire);
    setReel(this->getReel() * reel);
}

void Complexe::modulo(int imaginaire, int reel)
{
    std::cout<<sqrt(pow(this->getImaginaire() + imaginaire,2)+pow(this->getReel() + reel,2))<<std::endl;
}


Complexe &Complexe::operator=(const Complexe &source)
{
    _reel = source._reel;
    _imaginaire = source._imaginaire;
    return *this;
}


Complexe &Complexe::operator+=(const Complexe &c)
{
    _reel += c._reel;
    _imaginaire += c._imaginaire;
    return *this;
}

Complexe &Complexe::operator-=(const Complexe &c)
{
    _reel -= c._reel;
    _imaginaire -= c._imaginaire;
    return *this;
}

Complexe &Complexe::operator*=(const Complexe &c)
{
    double temp = _reel*c._reel - _imaginaire*c._imaginaire;
    _imaginaire = _imaginaire*c._imaginaire + _imaginaire*c._reel;
    _reel = temp;
    return *this;
}

Complexe &Complexe::operator/=(const Complexe &c)
{
    double norm = c._reel*c._reel + c._imaginaire*c._imaginaire;
    double temp = (_reel*c._reel + _imaginaire*c._imaginaire) / norm;
    _imaginaire = (-_reel*c._imaginaire + _imaginaire*c._reel) / norm;
    _reel = temp;
    return *this;
}

Complexe operator+(const Complexe &c1, const Complexe &c2)
{
    Complexe result = c1;
    return result += c2;
}

Complexe operator-(const Complexe &c1, const Complexe &c2)
{
    Complexe result = c1;
    return result -= c2;
}

Complexe operator*(const Complexe &c1, const Complexe &c2)
{
    Complexe result = c1;
    return result *= c2;
}

Complexe operator/(const Complexe &c1, const Complexe &c2)
{
    Complexe result = c1;
    return result /= c2;
}

Complexe operator<<(const Complexe &c1, const Complexe &c2)
{
    Complexe result = (c1+c2)*2;
    return result;
}

Complexe operator>>(const Complexe &c1, const Complexe &c2)
{
    Complexe result = (c1+c2)/2;
    return result;
}
