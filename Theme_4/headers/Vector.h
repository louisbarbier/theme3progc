//
// Created by Louis on 08/11/2020.
//

#ifndef TPDUOCPP_VECTOR_H
#define TPDUOCPP_VECTOR_H
#include <iostream>

class Vector {
    int * dynamic_table;
    int size;

    friend Vector operator+(Vector &,Vector &);
    friend Vector operator-(Vector &,Vector &);
    friend Vector operator/(Vector &,Vector &);
    friend Vector operator*(Vector &,Vector &);
    friend Vector operator<<(Vector &,Vector &);
    friend Vector operator>>(Vector &,Vector &);



public:
    Vector(const int *dynamicTable, int size);

    //Vector operator=(Vector &);

    Vector();

    void display();

    virtual ~Vector();

    int *getDynamicTable() const;

    void setDynamicTable(int *dynamicTable);

    int getSize() const;

    void setSize(int size);


};


#endif //TPDUOCPP_VECTOR_H
