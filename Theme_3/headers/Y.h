//
// Created by Louis on 07/11/2020.
//

#ifndef TPDUOCPP_Y_H
#define TPDUOCPP_Y_H
#include <iostream>

class Y {
    int * dynamic_table;
    int table_length;
public:
    Y(Y const &);

    Y();

    inline void display()
    {
        for(int i=0;i<table_length;++i)
        {
            std::cout<<dynamic_table[i]<<std::endl;
        }

    }

    inline int*  getTable()
    {
        return dynamic_table;
    }

    virtual ~Y();

};


#endif //TPDUOCPP_Y_H
