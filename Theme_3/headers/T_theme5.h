//
// Created by Louis on 07/11/2020.
//

#ifndef TPDUOCPP_T_THEME5_H
#define TPDUOCPP_T_THEME5_H
#include "O.h"

class T {
    O ** o_table;
    int l,c;
public:
    T(O **oTable, int l, int c);

    virtual ~T();
};


#endif //TPDUOCPP_T_THEME5_H
