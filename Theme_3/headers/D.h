//
// Created by Louis on 07/11/2020.
//

#ifndef TPDUOCPP_D_H
#define TPDUOCPP_D_H
#include <string>

class D {
    const std::string class_name;
public:
    D(const char *className);

    virtual ~D();

};


#endif //TPDUOCPP_D_H
