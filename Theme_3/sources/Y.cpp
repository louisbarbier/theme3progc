//
// Created by Louis on 07/11/2020.
//

#include <time.h>
#include "../headers/Y.h"

Y::Y(const Y & y) {
    Y::dynamic_table = new int[y.table_length];
    table_length = y.table_length;
    for(int i=0;i<y.table_length;++i)
    {
        dynamic_table[i] = y.dynamic_table[i];
    }
}

Y::Y() {
    std::cout<<"Constructor Y"<<std::endl;
    Y::dynamic_table = new int[10];
    table_length = 10;
    srand(time(NULL));
    for(int i=0;i<10;++i)
    {
        *(Y::dynamic_table + i) = rand() % 100;
    }
}

Y::~Y() {
    std::cout<<"Deleting Y"<<std::endl;
    delete [] dynamic_table;
}
