//
// Created by Louis on 07/11/2020.
//

#include "../headers/T_theme5.h"
#include "../../Theme_5/headers/U.h"
#include <iostream>

T::~T() {
    std::cout<<"Destructor object t"<<std::endl;
    for(int i = 0; i < l; ++i)
        delete[] o_table[i];
    delete [] o_table;
}

T::T(O **oTable, int l, int c) : o_table(oTable), l(l), c(c) {}

