//
// Created by Louis on 07/11/2020.
//
#include <iostream>
#include "../headers/D.h"

D::D(const char *className) : class_name(className) {}

D::~D() {
    std::cout<<"Destructor: "<< class_name <<std::endl;
}
