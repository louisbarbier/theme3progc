#include "../tp5.h"
#include <iostream>
#include "headers/Y.h"
#include "headers/D.h"
#include "headers/T_theme5.h"
int theme_1();

#ifdef THEME_3
int main() {
    theme_1();

    return 0;
}
#endif
int theme_1()
{
    Y e = Y();
    Y f = Y(e);

    //Step 1

    /**
     * J'imagine que le constructeur que l'on devait utiliser en premier lieu
     * réalisait une copie de l'adresse du pointeur.
     * Cependant pour réaliser un vrai constructeur par copie il faut copier les valeurs et non l'adresse du tableau.
     * C'est ce que j'ai fait pour mon propre constructeur
     */
    std::cout<<"First display"<<std::endl;
    std::cout<<"e:"<<std::endl;
    e.display();
    std::cout<<"f:"<<std::endl;
    f.display();
    std::cout<<"changing addr 1 value"<<std::endl;
    (e.getTable())[1] = 999;
    std::cout<<"e:"<<std::endl;
    e.display();
    std::cout<<"f:"<<std::endl;
    f.display();

    //Step 2
    {//Entrée du block
        D d_object = D("d_object");
    }
    D * d_dynamic = new D("d_dynamic");

    delete d_dynamic;
    /**
     * Le destructeur d'un pointeur est appelé lors du delete
     * Le destructeur d'un object est appelé à la fin du block
     *
     * Les objets ne sont pas détruits s'ils sont adressés manuellement
     */

    //Step 3
    const int i=2,c=2;
    O ** o = new O*[c];
    for(int j = 0; j < c; ++j)
        o[j] = new O[i];

    T t = T(o,i,2);

    /**
     * Les destructeurs de O sont appelés lors de l'appel du destructeur de T, juste avant qu'il soit détruit
     */
    std::cout<<"------Derniere partie-------"<<std::endl;
     Y* a = new Y();
    void * b = new Y();

    std::cout<<"Deleting a"<<std::endl;
    delete a;


    std::cout<<"Deleting b"<<std::endl;
    delete b;
    /**
     * DELETE NE PEUT PAS ETREE APPELE SUR LE TYPE VOID*
     * Donc ça ne fait rien
     */

    std::cout<<"a and b deleted"<<std::endl;
    std::cout<<"---------------------------------------"<<std::endl;

    return 1;
}
