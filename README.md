BARBIER Louis, FONTAINE Thomas, LSI1
---
# Réponses pour le TP5

## Comment tester les réponses?
- Aller dans tp5.h
- Changer la valeur de define:
* THEME_5
* THEME_3
* THEME_4
 
 **Certaines réponse sont écrites en commentaire**




# Enoncé TP5
# Thème 3 : new, new[], delete, delete[]
## Recopie et tableaux dynamiques
Créez une classe Y dont un des membres est un tableau dynamique (donc un pointeur).
- Créez deux objets e et f de la classe Y, en créant f à partir de e. Quel est le constructeur appelépour créer f ? - Modifiez un élément du tableau stocké dans e et affichez f. Qu'en déduisez-vous sur le constructeur utilisé pour créer f ?
- Ecrivez votre propre version de ce constructeur et testez-la. N'oubliez pas d'utiliser les opérateurs d'allocation et de libération dans les constructeurs et destructeurs !

## Un destructeur loquace
- Créez une classe D dont les constructeurs et le destructeur s'annoncent en écrivant un message.
- Ecrivez un programme créant des objets de classe D ainsi qu'un ou des pointeurs sur des objets de classe D. Quand les destructions d'objets ont-elles lieu ? 
- Trouvez un moyen de pouvoir visualiser tous les messages affichés par le destructeur. Y a-t-il toujours autant de destruction d'objets que de création d'objets ?

## Tableau 2D
- Créez une classe T stockant un tableau dynamique 2D comportant l lignes et c colonnes. 
Les éléments de ce tableau doivent être des objets d'une autre classe O. 
Quand les constructeurs et les destructeurs de O sont-ils appelés ?

## Opérateur delete et void*
 - En réutilisant la classe Y comportant un tableau dynamique, créez deux pointeurs a et b dans un programme principal, a étant de type Y* et de type void*. 
Les constructeurs et destructeurs de Y doivent faire des affichages pour indiquer qu'ils sont en cours d'exécution.
- Initialisez a et b en utilisant l'opérateur new, puis libérez a et b grâce à l'opérateur delete.
Que constatez-vous ?
# Thème 4 : Surcharge des opérateurs
En reprenant la classe complex, surchargez les opérateurs suivants : +,-,*,/, <<,>>Surchargez l'opérateur d'affectation =et testez tous ces opérateurs dans un programmeprincipal. Que peut-on dire maintenant de la classe complexe ?Créez une classe Vectordont un des membres est un tableau dynamique d'entiers.Surchargez les opérateurs +,-,*, <<, >>et =. Testez l'opérateur d'affectation en effectuant uneauto-affectation dans le programme principal.

# Thème 5 : composition et initialisations d'objets, membres statiques
Ecrivez un programme avec deux classes nommées A et B. Parmi les membres de la classe Bdoit se trouver un objet de la classe A nommé a.
Ecrivez le constructeur de la classe B,trouvez plusieurs manières d'initialiser le membre a.
Ecrivez une classe Sess qui attribue un numéro différent (et croissant) à chaque objet de cetteclasse.
A chaque fois qu'un objet de cette classe est créé ou détruit, afficher le nombre d'objetsde cette classe qui existent.Ecrivez un programme avec deux classes T et U. Parmi les membres de la classe T doit setrouver un pointeur vers un objet de la classe U. Ecrivez tous les constructeurs et destructeursdes classes T et U, testez-les dans un programme principal.